def buildJar() {
    echo 'Building the application...'
    sh 'mvn package'
}

def buildImage() {
    echo 'Building the image...'
    withCredentials([usernamePassword(credentialsId: 'docker-hub', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh 'docker build -t hesselotten/my-repo:jma-0.0.4 .'
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh 'docker push hesselotten/my-repo:jma-0.0.4'
    }
}

def deployApp() {
    echo 'Deploying the application...'
}

return this