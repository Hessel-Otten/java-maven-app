package com.example.javamavenapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class JavaMavenAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaMavenAppApplication.class, args);
    }

    @PostConstruct
    public void init()
    {
        Logger log = LoggerFactory.getLogger(JavaMavenAppApplication.class);
        log.info("Java app started");
    }

    public String getStatus() {
        return "OK";
    }
}
