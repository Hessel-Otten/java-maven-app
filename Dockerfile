FROM openjdk:8-jre-alpine

WORKDIR /usr/app

COPY ./target/java-maven-app-0.0.1-SNAPSHOT.jar .

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "java-maven-app-0.0.1-SNAPSHOT.jar"]